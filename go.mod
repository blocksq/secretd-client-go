module gitlab.com/blocksq/secretd-client-go

go 1.13

require (
	github.com/flynn/noise v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1
	github.com/ugorji/go/codec v1.1.7
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
